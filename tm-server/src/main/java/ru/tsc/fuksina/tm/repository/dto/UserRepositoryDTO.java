package ru.tsc.fuksina.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.repository.dto.IUserRepositoryDTO;
import ru.tsc.fuksina.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepositoryDTO extends AbstractRepositoryDTO<UserDTO> implements IUserRepositoryDTO {

    public UserRepositoryDTO(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        return entityManager.createQuery("SELECT m  FROM UserDTO m", UserDTO.class).getResultList();
    }

    @Nullable
    @Override
    public UserDTO findOneById(@NotNull final String id) {
        return entityManager.find(UserDTO.class, id);
    }

    @Nullable
    @Override
    public UserDTO findOneByLogin(@NotNull final String login) {
        return entityManager
                .createQuery("SELECT m FROM UserDTO m WHERE m.login = :login", UserDTO.class)
                .setParameter("login", login)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public  UserDTO findOneByEmail(@NotNull final String email) {
        return entityManager
                .createQuery("SELECT m FROM UserDTO m WHERE m.email = :email", UserDTO.class)
                .setParameter("email", email)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(entityManager.getReference(UserDTO.class, id));
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        entityManager.createQuery("DELETE FROM UserDTO m WHERE m.login = :login", UserDTO.class)
                .setParameter("login", login)
                .executeUpdate();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM UserDTO").executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return entityManager.createQuery("SELECT COUNT(m) = 1 FROM UserDTO m WHERE m.id = :id", Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(m) FROM UserDTO", Long.class)
                .getSingleResult();
    }

}
