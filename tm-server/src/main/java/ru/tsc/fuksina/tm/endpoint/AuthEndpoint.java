package ru.tsc.fuksina.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.fuksina.tm.api.service.IAuthService;
import ru.tsc.fuksina.tm.api.service.IServiceLocator;
import ru.tsc.fuksina.tm.dto.request.UserLoginRequest;
import ru.tsc.fuksina.tm.dto.request.UserLogoutRequest;
import ru.tsc.fuksina.tm.dto.request.UserShowProfileRequest;
import ru.tsc.fuksina.tm.dto.response.UserLoginResponse;
import ru.tsc.fuksina.tm.dto.response.UserLogoutResponse;
import ru.tsc.fuksina.tm.dto.response.UserShowProfileResponse;
import ru.tsc.fuksina.tm.dto.model.SessionDTO;
import ru.tsc.fuksina.tm.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.fuksina.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        check(request);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserShowProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserShowProfileRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final UserDTO user = getServiceLocator().getUserService().findOneById(session.getUserId());
        return new UserShowProfileResponse(user);
    }

}
