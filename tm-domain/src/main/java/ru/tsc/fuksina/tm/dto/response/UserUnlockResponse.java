package ru.tsc.fuksina.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class UserUnlockResponse extends AbstractResponse {
}
