package ru.tsc.fuksina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
public final class Task extends AbstractUserOwnedModel {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

}
