package ru.tsc.fuksina.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.model.IWBS;
import ru.tsc.fuksina.tm.enumerated.Status;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public final class ProjectDTO extends AbstractUserOwnedModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

}
